var myVictories = 0
var computerVictories = 0
function sumVictory(dataJson) {
  if (dataJson.victory == "Computer win") {
    computerVictories++;
  } else if (dataJson.victory == "I win") {
    myVictories++;
  }
}
var game = function(optionSel) {
  $.ajax({
    url: "rockpaperscissors/play",
    method: "GET",
    data: { "optionSel" : optionSel },
    dataType: "json",
    success: function(dataJson){
      $("#inData").append("You select a " + dataJson.myOptionSelDescription + "\r");
      $("#inData").append("Computer select a " + dataJson.computerOptionDesc + "\r");
      $("#inData").append(dataJson.victory + "\r");
      $("#inData").animate({scrollTop:$("#inData")[0].scrollHeight - $("#inData").height()},200);
      sumVictory(dataJson);
      $("#val_win").text(myVictories);
      $("#val_fail").text(computerVictories);
    }
  });
};

$('#rockSel').click(function() {
  game(0);
});
$('#paperSel').click(function() {
  game(1);
});
$('#scissorsSel').click(function() {
  game(2);
});
